import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import LogOutIcon from '@material-ui/icons/PowerSettingsNew';

class SimpleHeader extends React.Component {

    constructor(props) {
        super(props);
        this.props = props;
        this.setState({open: props.open});
        this.setState({logIn: props.logIn});
    }

    handleDrawerOpenClose = () => {
        this.setState({open: !this.state.open});
    };

    handleLogOut = () => {
      this.setState({logIn : !this.state.logIn});
    };

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="static" className={classNames(classes.appBar)}>
                    <Toolbar style={{background: "#0e1787"}}>
                        <IconButton color="inherit" aria-label="Open drawer" onClick={this.handleDrawerOpenClose}
                            className={classNames(classes.menuButton)} >
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="title" color="inherit" noWrap>
                            {this.props.title}
                        </Typography>
                        <IconButton color="inherit" aria-label="Log out" onClick={this.handleLogOut()}>
                            <LogOutIcon/>
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

SimpleHeader.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleHeader);

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: 440,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        background: "#0e1787"
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        background: '#ffffff',
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
});