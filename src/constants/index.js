export const ADD_POST = 'ADD_POST';
export const FETCH_ALL_POSTS = 'FETCH_ALL_POSTS';

export const ADD_COMMENT = 'ADD_COMMENT';
export const FETCH_ALL_COMMENTS = 'FETCH_ALL_COMMENTS';