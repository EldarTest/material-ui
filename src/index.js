import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/app/App';
import registerServiceWorker from './registerServiceWorker';
import 'typeface-roboto';
import './index.css';
import store from './store'
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';


ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App/>
        </Router>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
