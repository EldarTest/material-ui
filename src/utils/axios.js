import axios from 'axios';

 class HttpService {

    constructor(baseUrl, timeout, responseType) {
        this._apiHttpService = axios.create({
            baseURL: baseUrl,
            timeout: timeout,
            responseType: responseType
        });
    }

    get apiHttpService() {
        return this._apiHttpService;
    }

    set apiHttpService(value) {
        this._apiHttpService = value;
    }

}

// export default new HttpService("https://jsonplaceholder.typicode.com/", 2000, 'json');

export default axios.create({
    baseURL: "https://jsonplaceholder.typicode.com/",
    timeout: 2000,
    responseType: 'json'
});