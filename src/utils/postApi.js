import HttpService from './axios';

export class PostService {

    constructor() {
        this.server = HttpService;
        this.endPoint = 'posts';
    }

    getAll = () => {
        let dataResponse = null;
        this.server.apiHttpService.get(`${this.endPoint}`)
            .then(response => {
                dataResponse = JSON.parse(response.data)
            })
            .catch(e => {
                console.log(e);
                throw e;
            });
        return dataResponse;
    };

    getById = id => {
        let dataResponse = null;
        if (id !== undefined) {
            this.server.apiHttpService.get(`${this.endPoint}/${id}`)
                .then(response => {
                    dataResponse = JSON.parse(response.data)
                })
                .catch(e => {
                    console.log(e);
                    throw e;
                });
        }
        return dataResponse;
    }

}

export default new PostService();