import axios from '../utils/axios';
import * as actionConstants from '../constants';

export const getPosts = () => dispatch => {
    axios.get("/posts")
        .then(response => {
            dispatch({type: actionConstants.FETCH_ALL_POSTS, payload: [...response.data]});
        })
        .catch(e => console.log(e));
};


export const getComments = () => dispatch => {
    axios.get("/comments")
        .then(response => {
            dispatch({type: actionConstants.FETCH_ALL_COMMENTS, payload: [...response.data]});
        })
        .catch(e => console.log(e));
};