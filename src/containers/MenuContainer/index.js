import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import PowerIcon from '@material-ui/icons/PowerSettingsNew';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ReportIcon from '@material-ui/icons/FilterNone';
import UsersIcon from '@material-ui/icons/People';
import ServiceIcon from '@material-ui/icons/BusinessCenter';
import TemplateMvIcon from '@material-ui/icons/AccountBalance';
import EventIcon from '@material-ui/icons/EventNote';
import SettingIcon from '@material-ui/icons/Settings';
import AdminSettingIcon from '@material-ui/icons/PermDataSetting';
import SlotsIcon from '@material-ui/icons/DateRange';
import EsiaIcon from '@material-ui/icons/VpnKey';
import OrderIcon from '@material-ui/icons/ThumbUp';
import MvRequestIcon from '@material-ui/icons/Public';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import PostsApi from '../../utils/postApi';
import {Link, Route, Switch} from 'react-router-dom';
import Posts from "../MainContent/Posts";
import Comments from '../MainContent/Comments';

class MiniDrawer extends React.Component {

    state = {
        open: false,
        logIn: true,
        openInbox: false,
        openAdministration: false,
        data: {}
    };

    handleDrawerOpen = () => {
        this.setState({open: true});
    };

    handleDrawerClose = () => {
        this.setState({open: false});
    };

    handleLogOut = () => {
        this.setState({logIn: !this.state.logIn});
    };

    handleClickReport = () => {
        this.setState(state => ({openReports: !state.openReports}));
    };

    handleClickAdministration = () => {
        this.setState(state => ({openAdministration: !state.openAdministration}));
    };

    render() {
        const {classes, theme} = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="absolute"
                        className={classNames(classes.appBar, this.state.open && classes.appBarShift)}>
                    <Toolbar disableGutters={false} style={{background: "#0e1787"}}>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerOpen}
                            className={classNames(classes.menuButton, this.state.open && classes.hide)}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="title" color="inherit" noWrap>
                            IT - Universe
                        </Typography>
                        <IconButton color="inherit" aria-label="Log out" onClick={this.handleLogOut}
                                    className={classNames(classes.menuButton, {/*this.state.open && classes.hide*/})}>
                            <PowerIcon/>
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
                    }}
                    open={this.state.open}
                >
                    <div className={classes.toolbar}>
                        <IconButton onClick={this.handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon/> : <ChevronLeftIcon/>}
                        </IconButton>
                    </div>
                    <Divider/>
                    <List component="nav">
                        <Link to="/posts">
                            <ListItem button>
                                <ListItemIcon>
                                    <OrderIcon/>
                                </ListItemIcon>
                                <ListItemText inset primary="Заявления"/>
                            </ListItem>
                        </Link>
                        <Link to="/comments">
                            <ListItem button>
                                <ListItemIcon>
                                    <MvRequestIcon/>
                                </ListItemIcon>
                                <ListItemText inset primary="МВ-запросы"/>
                            </ListItem>
                        </Link>
                        <ListItem button>
                            <ListItemIcon>
                                <SlotsIcon/>
                            </ListItemIcon>
                            <ListItemText inset primary="Запись на прием"/>
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <EsiaIcon/>
                            </ListItemIcon>
                            <ListItemText inset primary="ЕСИА"/>
                        </ListItem>
                        <ListItem button onClick={this.handleClickReport}>
                            <ListItemIcon>
                                <ReportIcon/>
                            </ListItemIcon>
                            <ListItemText inset primary="Отчеты"/>
                            {this.state.openReports ? <ExpandLess/> : <ExpandMore/>}
                        </ListItem>
                        <Collapse in={this.state.openReports} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem button className={classes.nested}>
                                    <ListItemText inset primary="Все услуги"/>
                                </ListItem>
                                <ListItem button className={classes.nested}>
                                    <ListItemText inset primary="Элекронные услуги"/>
                                </ListItem>
                            </List>
                        </Collapse>
                        <ListItem button onClick={this.handleClickAdministration}>
                            <ListItemIcon>
                                <AdminSettingIcon/>
                            </ListItemIcon>
                            <ListItemText inset primary="Администрирование"/>
                            {this.state.openAdministration ? <ExpandLess/> : <ExpandMore/>}
                        </ListItem>
                        <Collapse in={this.state.openAdministration} timeout="auto" unmountOnExit>
                            <List component="div" disablePadding>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <UsersIcon/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Пользователи"/>
                                </ListItem>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <ServiceIcon/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Услуги"/>
                                </ListItem>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <TemplateMvIcon/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Шаблоны МВ-запросов"/>
                                </ListItem>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <EventIcon/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="События"/>
                                </ListItem>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <SettingIcon/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Настройки"/>
                                </ListItem>
                            </List>
                        </Collapse>
                    </List>
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    <Switch>
                        <Route path="/posts" component={Posts}/>
                        <Route path="/comments" component={Comments}/>
                    </Switch>
                </main>
            </div>
        );
    }
}

MiniDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};


const styles = theme => ({
    root: {
        flexGrow: 1,
        // height: 440,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        background: "#0e1787"
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        background: '#ffffff',
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
    },
});

const drawerWidth = 280;

export default withStyles(styles, {withTheme: true})(MiniDrawer);

