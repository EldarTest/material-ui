import React, {Component} from 'react';
import MenuContainer from "../../containers/MenuContainer";

class App extends Component {
    render() {
        return (
            <div>
                <MenuContainer/>
            </div>
        );
    }
}

export default App;
