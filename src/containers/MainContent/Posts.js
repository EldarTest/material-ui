import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import compose from 'recompose/compose';
import {connect} from "react-redux";
import * as actions from '../../actions';

class PostsTable extends React.Component {

    componentWillMount() {
        this.props.onGetAllPosts();
    }

    render() {
        const {classes, rowsPosts} = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>id</TableCell>
                            <TableCell>userId</TableCell>
                            <TableCell>title</TableCell>
                            <TableCell>body</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rowsPosts.map(row => {
                            return (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell numeric>{row.userId}</TableCell>
                                    <TableCell numeric>{row.title}</TableCell>
                                    <TableCell numeric>{row.body}</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }

}

PostsTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});


// export default withStyles(styles)(SimpleTable);
export default compose(
    withStyles(styles),
    connect(
        state => ({rowsPosts: state.posts}),
        dispatch => ({
            onGetAllPosts: () => {
                dispatch(actions.getPosts());
            }
        })
    ),
)(PostsTable);